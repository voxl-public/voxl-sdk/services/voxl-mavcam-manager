# voxl-mavcam-manager

Implementation of the Mavlink camera protocol

This README covers building this package. The voxl-mavcam-manager user manual is located [here](https://docs.modalai.com/voxl-mavcam-manager/)

## Build Instructions

#### Prerequisites

* voxl-cross for voxl1 and voxl2 builds: https://gitlab.com/voxl-public/utilities/voxl-docker/-/tree/dev


#### Clean out all old artifacts

```bash
~/git/voxl-mavcam-manager$ ./clean.sh
```

#### Build for Voxl1

1) Launch Docker and install all required dependencies

```bash
~/git/voxl-mavcam-manager$ voxl-docker -i voxl-cross:V2.5
voxl-cross(2.5):~(master)(0.1.0)$ ./install_build_deps.sh apq8096 staging
```

2) Compile inside the docker.

```bash
voxl-cross(2.5):~(master)(0.1.0)$ ./build.sh apq8096
```

3) Make an ipk package inside the docker.

```bash
voxl-cross(2.5):~(master)(0.1.0)$ ./make_package.sh ipk
Package Name:  voxl-mavcam-manager
version Number:  0.2.0
Consolidate compiler generated dependencies of target voxl-mavcam-manager
[100%] Built target voxl-mavcam-manager
Install the project...
-- Install configuration: ""
-- Installing: ../pkg/data/usr/bin/voxl-mavcam-manager
/home/root
starting building IPK package
ar: creating voxl-mavcam-manager_0.2.0.ipk
DONE
```

This will make a new voxl-mavcam-manager_x.x.x.ipk file in your working directory. The name and version number came from the pkg/control/control file. If you are updating the package version, edit it there.

#### Build for Voxl2

1) Launch Docker and install all required dependencies

```bash
~/git/voxl-mavcam-manager$ voxl-docker -i voxl-cross:V2.5
voxl-cross(2.5):~(master)(0.1.0)$ ./install_build_deps.sh qrb5165 staging
```

2) Compile inside the docker.

```bash
voxl-cross(2.5):~(master)(0.1.0)$ ./build.sh qrb5165
```

3) Make a deb package inside the docker.

```
voxl-cross(2.5):~(dev)(0.2.0)$ ./make_package.sh deb
Package Name:  voxl-mavcam-manager
version Number:  0.2.0
Consolidate compiler generated dependencies of target voxl-mavcam-manager
[100%] Built target voxl-mavcam-manager
Install the project...
-- Install configuration: ""
-- Installing: ../pkg/data/usr/bin/voxl-mavcam-manager
/home/root
starting building Debian Package
dpkg-deb: building package 'voxl-mavcam-manager' in 'voxl-mavcam-manager_0.2.0_arm64.deb'.
DONE
```

This will make a new voxl-mavcam-manager_x.x.x.deb file in your working directory. The name and version number came from the pkg/control/control file. If you are updating the package version, edit it there.


## Deploy to VOXL1 / VOXL2

You can now push the package to the board and install with the package manager however you like.
To do this over ADB, you may use the included helper script: install_on_voxl.sh.

Do this OUTSIDE of docker as your docker image probably doesn't have usb permissions for ADB.

```bash
$ ./deploy_to_voxl.sh
searching for ADB device
adb device found
dpkg detected
voxl-mavcam-manager_0.2.0_arm64.deb: 1 file pushed. 4.9 MB/s (49584 bytes in 0.010s)
Selecting previously unselected package voxl-mavcam-manager.
(Reading database ... 79035 files and directories currently installed.)
Preparing to unpack /data/voxl-mavcam-manager_0.2.0_arm64.deb ...
Unpacking voxl-mavcam-manager (0.2.0) ...
Setting up voxl-mavcam-manager (0.2.0) ...
DONE
```
